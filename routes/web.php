<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::namespace('Admin')->prefix('admins')->group(function () {
    Route::get('login', 'LoginController@getLogin')->name('admin.getLogin');
    Route::post('login', 'LoginController@Login')->name('admin.login');
    Route::get('logout', 'LoginController@logout')->name('admin.logout');
});


Route::namespace('Admin')->prefix('admins')->middleware('checklogin::class')->group(function () {
    Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');

    Route::resource('category', 'CategoryController');

    Route::resource('product', 'ProductController')->except(['show']);
    Route::get('product/search', 'ProductController@search')->name('product.search');

    Route::resource('role', 'RoleController')->except(['show']);

    Route::resource('permission', 'PermissionController')->except(['show', 'edit']);

    Route::resource('user', 'UserController');
});
