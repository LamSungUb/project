<?php

namespace App\Repositories;

use App\Product;
use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;

class ProductRepository extends BaseRepository
{
    public function model()
    {
        return Product::class;
    }
}
