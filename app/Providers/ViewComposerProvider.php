<?php

namespace App\Providers;

use App\Category;
use App\Permission;
use App\Role;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['admin.product.index', 'admin.product.modal_product', 'admin.product.table_product'], function ($view) {
            $view->with('categories', Category::all());
        });
        View::composer(['admin.user.index', 'admin.user.form_user'], function ($view) {
            $view->with('roles', Role::all());
        });
        View::composer(['admin.role.index', 'admin.role.form_role'], function ($view) {
            $view->with('permissions', Permission::all());
        });
    }
}
