<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'price' => 'required|numeric',
            'category_id' => 'required',
            'description' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên sản phẩm là trường bắt buộc',
            'image.image' => 'Ảnh không đúng định dạng',
            'image.mimes' => 'Ảnh phải có đuôi .jpeg, .png, .jpg, .gif, .svg',
            'price.required' => 'Giá không được để chống',
            'price.numeric' => 'Giá phải là kiểu số',
            'category_id.required' => 'Danh mục không được để chống',
            'description.required' => 'Mô tả không được để chống',
        ];
    }
}
