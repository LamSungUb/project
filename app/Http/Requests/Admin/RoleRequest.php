<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'checkbox' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên vai trò là trường bắt buộc',
            'name.string' => 'Tên vai trò không đúng định dạng',
            'checkbox.required' => 'Bắt buộc phải chọn quyền'
        ];
    }
}
