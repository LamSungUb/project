<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateProductRequest;
use App\Http\Requests\Api\UpdateProductRequest;
use App\Http\Resources\ProductCollection;
use App\Product;
use App\Http\Controllers\Controller;
use App\Http\Resources\Product as ProductResource;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return new ProductCollection($products);
    }

    public function store(CreateProductRequest $request)
    {
        $product = Product::create($request->all());
        return response()->json([
            'status_code' => 200,
            'message' => 'Thêm thành công',
            'data' => $product,
        ]);
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);
        return response()->json([
            'status_code' => 200,
            'message' => 'Hiển thị 1 sản phẩm',
            'data' => new ProductResource($product),
        ]);
    }

    public function update(UpdateProductRequest $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->update($request->all());
        return response()->json([
            'status_code' => 200,
            'message' => 'Cập nhập thành công',
            'data' => $product
        ]);
    }

    public function destroy($id)
    {
        Product::findOrFail($id)->delete();
        return response()->json([
            'status_code' => 200,
            'message' => 'Xóa thành công',
            'data' => true,
        ]);
    }
}
