<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RoleRequest;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();

        return view('admin.role.index', [
            'roles' => $roles,
        ]);
    }

    public function store(RoleRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = [
                'name' => $request->input('name'),
            ];
            $permissions = $request->input('checkbox');
            $role = Role::create($input);
            $role->permissions()->attach($permissions);
            DB::commit();

            return back();
        } catch (\Exception $exception) {
            DB::rollBack();
        }
    }

    public function edit($id)
    {
        $list_permissions = DB::table('permission_role')->where('role_id', $id)->pluck('permission_id');
        $role = Role::findOrFail($id);

        return view('admin.role.form_role', [
            'role' => $role,
            'list_permissions' => $list_permissions
        ]);
    }

    public function update(RoleRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            Role::where('id', $id)->update([
                'name' => $request->input('name'),
            ]);
            DB::table('permission_role')->where('role_id', $id)->delete();
            $role = Role::find($id);
            $role->permissions()->attach($request->input('checkbox'));
            DB::commit();

            return back();
        } catch (\Exception $exception) {
            DB::rollBack();
        }
    }


    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $role = Role::find($id);
            $role->delete();
            $role->permissions()->detach();
            DB::commit();

            return back();
        } catch (\Exception $exception) {
            DB::rollBack();
        }

    }
}
