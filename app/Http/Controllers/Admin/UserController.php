<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CreateUserRequest;

use App\Http\Requests\Admin\UpdateUserRequest;
use App\User;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    public function index()
    {
        $users = User::all();
        return view('admin.user.index', [
            'users' => $users,
        ]);
    }

    public function store(CreateUserRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
            ];
            $roles = $request->input('checkbox');
            $user = User::create($input);
            $user->roles()->attach($roles);
            DB::commit();

            return back();
        } catch (\Exception $exception) {
            DB::rollBack();
        }

    }

    public function edit($id)
    {
        $list_roles = DB::table('role_user')->where('user_id', $id)->pluck('role_id');
        $user = User::findOrFail($id);

        return view('admin.user.form_user', [
            'user' => $user,
            'list_roles' => $list_roles
        ]);
    }

    public function update(UpdateUserRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            User::where('id', $id)->update([
                'name' => $request->input('name'),
            ]);
            DB::table('role_user')->where('user_id', $id)->delete();
            $user = User::find($id);
            $user->roles()->attach($request->input('checkbox'));
            DB::commit();

            return back();
        } catch (\Exception $exception) {
            DB::rollBack();
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $user = User::find($id);
            $user->delete();
            $user->roles()->detach();
            DB::commit();

            return back();
        } catch (\Exception $exception) {
            DB::rollBack();
        }
    }
}
