<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PermissionRequest;
use App\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function index()
    {
        $permissions = Permission::all();

        return view('admin.permission.index', [
            'permissions' => $permissions,
        ]);
    }

    public function store(PermissionRequest $request)
    {
        Permission::create($request->all());

        return back();
    }

    public function update(PermissionRequest $request, $id)
    {
        Permission::findOrFail($id)->update($request->all());

        return back();
    }

    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);
        $permission->delete();

        return back();

    }
}
