<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductRequest;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('id', 'DESC')->paginate(3);

        return view('admin.product.index', [
            'products' => $products,
        ]);
    }

    public function store(ProductRequest $request)
    {
        $input = $request->all();
        if ($request->hasFile('image')) {
            $input['image'] = time() . '.' . $request->image->getClientOriginalName();
            $request->image->move(public_path('admin/images'), $input['image']);
        } else {
            $input['image'] = "image";
        }
        Product::create($input);
        $products = Product::orderBy('id', 'DESC')->paginate(3);

        return view('admin.product.table_product', [
            'products' => $products,
        ]);
    }

    public function edit($id)
    {
        $product = Product::find($id);

        return view('admin.product.modal_product', [
            'product' => $product,
        ]);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $product = Product::find($id);

        if ($request->hasFile('image')) {
            if ($product->image != $request->image->getClientOriginalName()) {
                unlink('admin/images/' . $product->image);
            }
            $input['image'] = time() . '.' . $request->image->getClientOriginalName();
            $request->image->move(public_path('admin/images'), $input['image']);
        }

        $product->update($input);
        $products = Product::orderBy('id', 'DESC')->paginate(3);

        return view('admin.product.table_product', [
            'products' => $products,
        ]);
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        if ($product->image) {
            unlink('admin/images/' . $product->image);
        }
        $product->delete();
        $products = Product::orderBy('id', 'DESC')->paginate(3);

        return view('admin.product.table_product', [
            'products' => $products,
        ]);
    }

    public function search(Request $request)
    {
        $keyword = $request->get('keyword');
        if ($keyword) {
            $products = Product::where('name', 'LIKE', "%$keyword%")->orderBy('id', 'DESC')->paginate(3);
            return view('admin.product.table_product', [
                'products' => $products,
            ]);
        }
    }
}
