<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryRequest;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('admin.category.index', [
            'categories' => $categories,
        ]);
    }

    public function store(CategoryRequest $request)
    {
        Category::create($request->all());
        return redirect()->back();
    }

    public function update(CategoryRequest $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->update($request->all());
        return redirect()->back();
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $category->products()->delete();
        $category->delete();

        return redirect()->back();
    }
}
