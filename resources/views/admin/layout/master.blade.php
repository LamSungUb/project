<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <base href="{{ asset('admin') }}">
    <link rel="stylesheet" href="admin/login/css/login_style.css">
    <link rel="stylesheet" href="admin/dashboard/css/dashboard.css">
    @yield('css')
    <style>
        #sidebarMenu a:hover {
            color: #007bff;
        }
    </style>
    <title> @yield('title') </title>
</head>
<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    <b class="navbar-brand col-md-3 col-lg-2 mr-0 px-3">Logo</b>
    <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse"
            data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    @if(Auth::check())
        <div class="form-control form-control-dark">
            <marquee>Welcome <strong>{{ Auth::user()->name }}</strong> to the admin page</marquee>
        </div>
    @endif
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="{{ route('admin.logout') }}">Logout</a>
        </li>
    </ul>
</nav>
<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="sidebar-sticky pt-3">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('admin.dashboard') }}">
                            <i class="fas fa-tachometer-alt"></i> Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('user.index') }}">
                            <span data-feather="users"></span>
                            <i class="fas fa-users"></i> Users
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('role.index') }}">
                            <span data-feather="users"></span>
                            <i class="fas fa-user-tag"></i> Roles
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('permission.index') }}">
                            <span data-feather="users"></span>
                            <i class="fas fa-user-friends"></i> Permissions
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('category.index') }}">
                            <span data-feather="shopping-cart"></span>
                            <i class="fas fa-tasks"></i> Categories
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('product.index') }}">
                            <span data-feather="users"></span>
                            <i class="fab fa-raspberry-pi"></i> Products
                        </a>
                    </li>
                </ul>
            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">

        <!-- content -->
        @yield('content')
        <!-- end content -->

        </main>
    </div>
</div>

{{--<script type="text/javascript" src="https://code.jquery.com/jquery-latest.pack.js"></script>--}}
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"></script>
<script src="admin/ajax/ajax.js"></script>
@yield('script')
</body>
</html>
