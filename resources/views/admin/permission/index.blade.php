@extends('admin.layout.master')
@section('title','permission')
@section('content')
    <div class="mt-2">
        <div class="row">
            <h3 class="col-6">Permission</h3>
            <div class="col-6">
                <form class="form-inline" method="POST" action="{{ route('permission.store') }}">
                    @csrf
                    <div class="form-group mb-2 mr-2">
                        <label for="name" class="sr-only">Name</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Create</button>
                </form>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only" style="font-size: 35px">Close</span>
                </button>
            </div>
        @endif
        <div id="table_role">
            <table class="table table-hover table-bordered">
                <thead class="thead">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach( $permissions as $permission)
                    <tr>
                        <th>{{ $permission->id }}</th>
                        <td>{{ $permission->name }}</td>
                        <td class="d-flex">
                            <!-- Button trigger modal -->
                            <button class="btn btn-success mr-2" data-toggle="modal" data-target="#edit{{ $permission->id }}">Edit</button>
                            <!-- Modal -->
                            <div class="modal fade" id="edit{{ $permission->id }}" tabindex="-1"
                                 aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form method="POST" action="{{ route('permission.update',$permission->id) }}">
                                            @csrf
                                            {!! @method_field('PUT') !!}
                                            <div class="modal-body">
                                                <div class="form-group mb-2 mr-2">
                                                    <input type="text" name="name" value="{{ $permission->name }}" class="form-control">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Update</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal"> Close </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <button class="btn btn-danger" data-toggle="modal" data-target="#del{{ $permission->id }}" > Del </button>
                            <div class="modal fade" id="del{{ $permission->id }}" tabindex="-1"
                                 aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form method="POST" action="{{ route('permission.destroy',$permission->id) }}">
                                            @csrf
                                            {!! @method_field('DELETE') !!}
                                            <div class="modal-body">
                                                <h5 class="flex-center">Do you want to delete</h5>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">DELETE</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal"> Close </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
