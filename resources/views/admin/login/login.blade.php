@extends('admin.layout.template')
@section('title','login')
@section('content')
    <div class="container">
        <div class="row justify-content-center full-height flex-center position-ref">
            <div class="col-lg-4 border-0 shadow-lg">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">Welcome!</h1>
                    </div>
                    <form class="user" method="post" action="{{ route('admin.login') }}">
                        @csrf
                        @if (Session::has('warning'))
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                {{Session::get('warning')}}
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only" style="font-size: 35px">Close</span>
                                </button>
                            </div>
                        @endif
                        <div class="form-group">
                            <input type="email" name="email" class="form-control form-control-user" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control form-control-user"
                                   placeholder="Password">
                        </div>
                        <button type="submit" class="btn btn-primary btn-user btn-block">
                            Login
                        </button>
                    </form>
                    <hr>
                    <div class="text-center">
                        <a class="small" href="">Forgot password?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
