@extends('admin.layout.master')
@section('title','role')
@section('content')
    <div class="my-3">
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only" style="font-size: 35px">Close</span>
                </button>
            </div>
        @endif
        <form method="POST" action="{{ route('role.update',$role->id) }}">
            @csrf
            {!! @method_field('PUT') !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" value="{{ $role->name }}" name="name">
                        </div>
                    </div>
                    <div class="form-group col-6">
                        <p>Permission</p>
                        @foreach($permissions as $permission)
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox"
                                       {{ $list_permissions->contains($permission->id) ? "checked" : "" }} name="checkbox[]"
                                       value="{{ $permission->id }}" class="custom-control-input"
                                       id="permission{{ $permission->id }}">
                                <label class="custom-control-label"
                                       for="permission{{ $permission->id }}">{{ $permission->name }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="{{ route('role.index') }}" type="button" class="btn btn-secondary">Back</a>
            </div>
        </form>
    </div>
@endsection
