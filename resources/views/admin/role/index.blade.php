@extends('admin.layout.master')
@section('title','role')
@section('content')
    <div>
        <div class="row my-3">
            <div class="col-6">
                <h3>Role</h3>
            </div>
            <div class="col-6">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal"> Create
                </button>
                <!-- Modal -->
                <div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content container">
                            <form method="POST" action="{{ route('role.store') }}">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input type="text" class="form-control" name="name">
                                            </div>
                                        </div>
                                        <div class="form-group col-6">
                                            <p>Permission</p>
                                            @foreach($permissions as $permission)
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" name="checkbox[]" value="{{ $permission->id }}" class="custom-control-input" id="permission{{ $permission->id }}">
                                                <label for="permission{{ $permission->id }}" class="custom-control-label">{{ $permission->name }}</label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only" style="font-size: 35px">Close</span>
                </button>
            </div>
        @endif
        <div>
            <table class="table table-hover table-responsive-sm table-responsive-md ">
                <thead class="thead">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Permission</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($roles as $role)
                    <tr>
                        <th>{{ $role->id }}</th>
                        <td>{{ $role->name }}</td>
                        <td>
                            @foreach($role->permissions as $permission)
                                {{ $permission->name }},
                            @endforeach
                        </td>
                        <td class="d-flex">
                            <!-- Button trigger modal -->
                            <a href="{{ route('role.edit',$role->id) }}" type="button" class="btn btn-success mr-2"> Edit </a>
                            <button data-toggle="modal" data-target="#delete{{ $role->id }}" class="btn btn-danger btn-del"> Del</button>
                            <div class="modal fade" id="delete{{ $role->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ route('role.destroy',$role->id) }}" method="POST">
                                            {!! @method_field('DELETE') !!}
                                            @csrf
                                            <div class="modal-body">
                                                <h5> Do you want to delete</h5>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Delete</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
