@extends('admin.layout.master')
@section('title','product')
@section('content')
    <div class="row my-3">
        <h3 class="col-3">Product</h3>
        <div class="col-3" id="modal">
            <!-- Button modal create-->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_create">
                Create
            </button>
            <!-- Modal create-->
            <div class="modal fade " id="modal_create" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <form enctype="multipart/form-data" id="form_create">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="container mt-4 mb-5">
                                    <div class="row">
                                        <div class="col-6 form-group">
                                            <div>
                                                <label for="product_name">Name <sup
                                                        class="text-danger">(*)</sup></label>
                                                <input type="text" id="name" name="name" class="form-control" required>
                                            </div>
                                            <div>
                                                <label for="price">Price <sup class="text-danger">(*)</sup></label>
                                                <input type="text" id="price" name="price" class="form-control"
                                                       required>
                                            </div>
                                            <div>
                                                <label for="category">Category <sup
                                                        class="text-danger">(*)</sup></label>
                                                <select id="category_id" name="category_id" class="custom-select"
                                                        required>
                                                    @foreach($categories as $category)
                                                        <option
                                                            value="{{ $category->id }}">{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6 form-group">
                                            <div>
                                                <label for="image" class="col-form-label">Image <sup
                                                        class="text-danger">(*)</sup></label>
                                                <div>
                                                    <input type="file" accept="image/*" id="image" name="image" required
                                                           onchange="loadFile(event)" style="display: none;">
                                                    <img height="120" width="120" id="output">
                                                    <label class="form-label" for="image" style="cursor: pointer;">
                                                        <p class="btn btn-outline-success">
                                                            <i class="fa fa-upload"></i>
                                                        </p>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="description">Description <sup class="text-danger">(*)</sup></label>
                                                <textarea id="description" name="description" class="form-control"
                                                          required rows="3"></textarea>
                                            </div>
                                        </div>
                                        <div>
                                            <p>Note: <i class="text-danger">(*) is a required field</i></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button class="btn btn-primary" type="button" id="btn_create"
                                        data-href="{{ route('product.store') }}">Create
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-6">
            <form class="form" >
                <input class="form-control" data-href={{ route('product.search') }} type="search" name="search" id="search" placeholder="Search">
            </form>
        </div>
    </div>

    <div id="tbl_result">
        <table class="table table-hover table-responsive-sm table-responsive-md ">
            <thead class="thead">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Image</th>
                <th scope="col">Category</th>
                <th scope="col">Price</th>
                <th scope="col">Description</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <th>{{ $product->id }}</th>
                    <td>{{ $product->name }}</td>
                    <td>
                        <img src="{{ asset('admin/images/'.$product->image) }}" style="width: 80px;height: 80px">
                    </td>
                    <td>{{ $product->category->name }}</td>
                    <td>{{ $product->price }}</td>
                    <td>{{ $product->description }}</td>
                    <td class="d-flex">
                        <button class="btn btn-success mr-2 btn-edit"
                                data-href="{{ route('product.edit',$product->id) }}" data-toggle="modal"> Edit
                        </button>
                        <button class="btn btn-danger btn-del" value="{{ $product->id }}"
                                data-href="{{ route('product.destroy', $product->id) }}"> Del
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $products->links() }}
    </div>

@endsection

@section('script')
    <script src="{{ asset('admin/product/js/product.js') }}"></script>
@endsection
