<table class="table table-hover table-responsive-sm table-responsive-md ">
    <thead class="thead">
    <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Image</th>
        <th scope="col">Category</th>
        <th scope="col">Price</th>
        <th scope="col">Description</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
        <tr>
            <th>{{ $product->id }}</th>
            <td>{{ $product->name }}</td>
            <td>
                <img src="{{ asset('admin/images/'.$product->image) }}" style="width: 80px;height: 80px">
            </td>
            <td>{{ $product->category->name }}</td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->description }}</td>
            <td class="d-flex">
                <button class="btn btn-success mr-2 btn-edit" data-href="{{ route('product.edit',$product->id) }}" data-toggle="modal" > Edit</button>
                <button class="btn btn-danger btn-del" value="{{ $product->id }}" data-href="{{ route('product.destroy', $product->id) }}"> Del</button>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $products->links() }}
