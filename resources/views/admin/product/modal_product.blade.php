<!-- Button modal create-->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_update"> Create</button>
<div class="modal fade " id="modal_update" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <form enctype="multipart/form-data" id="form_update">
            {!! method_field('PUT') !!}
            <div class="modal-content">
                <div class="modal-body">
                    <div class="container mt-4 mb-5">
                        <div class="row">
                            <div class="col-6 form-group">
                                <div>
                                    <label for="product_name">Name <sup class="text-danger">(*)</sup></label>
                                    <input value="{{ $product->name }}" type="text" id="name" name="name"
                                           class="form-control" required>
                                </div>
                                <div>
                                    <label for="price">Price <sup class="text-danger">(*)</sup></label>
                                    <input value="{{ $product->price }}" type="text" id="price" name="price"
                                           class="form-control" required>
                                </div>
                                <div>
                                    <label for="category">Category <sup class="text-danger">(*)</sup></label>
                                    <select id="category_id" name="category_id" class="custom-select" required>
                                        <option
                                            value="{{ $product->category->id }}">{{ $product->category->name }}</option>
                                        @foreach($categories as $category)
                                            @if(!$product->category->id)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-6 form-group">
                                <div>
                                    <label for="image" class="col-form-label">Image <sup
                                            class="text-danger">(*)</sup></label>
                                    <div>
                                        <input type="file" accept="image/*" id="image" name="image" required
                                               onchange="loadFile(event)" style="display: none;">
                                        <img height="120" width="120" id="output"
                                             src="{{ asset('admin/images/'.$product->image) }}">
                                        <label class="form-label" for="image" style="cursor: pointer;">
                                            <p class="btn btn-outline-success">
                                                <i class="fa fa-upload"></i>
                                            </p>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description">Description<sup class="text-danger">(*)</sup></label>
                                    <textarea id="description" name="description" class="form-control" required
                                              rows="3">{{ $product->description }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-primary" type="button" id="btn_update"
                            data-href="{{ route('product.update',$product->id) }}">Update
                    </button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>
