@extends('admin.layout.master')
@section('title','user')
@section('content')
  <div class="my-3">
      @if ($errors->any())
          <div class="alert alert-danger alert-dismissible" role="alert">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  <span class="sr-only" style="font-size: 35px">Close</span>
              </button>
          </div>
      @endif
      <form method="POST" action="{{ route('user.update',$user->id) }}">
          @csrf
          {!! @method_field('PUT') !!}
          <div class="modal-body">
              <div class="row">
                  <div class="col-6">
                      <div class="form-group">
                          <label for="name">Name</label>
                          <input type="text" class="form-control" value="{{ $user->name }}" name="name">
                      </div>
                  </div>
                  <div class="form-group col-6">
                      <p>Role</p>
                      @foreach($roles as $role)
                          <div class="custom-control custom-checkbox">
                              <input type="checkbox" {{ $list_roles->contains($role->id) ? "checked" : "" }} name="checkbox[]" value="{{ $role->id }}" class="custom-control-input" id="role{{ $role->id }}">
                              <label class="custom-control-label" for="role{{ $role->id }}">{{ $role->name }}</label>
                          </div>
                      @endforeach
                  </div>
              </div>
          </div>
          <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Update</button>
              <a href="{{ route('user.index') }}" type="button" class="btn btn-secondary" >Back</a>
          </div>
      </form>
  </div>
@endsection
