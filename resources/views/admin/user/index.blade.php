@extends('admin.layout.master')
@section('title','user')
@section('content')
    <div>
        <div class="row my-3">
            <div class="col-6">
                <h3>User</h3>
            </div>
            <div class="col-6">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_create_user"> Create
                </button>
                <!-- Modal -->
                <div class="modal fade" id="modal_create_user" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content container">
                            <form method="POST" action="{{ route('user.store') }}">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="name">Name</label>
                                                <input type="text" class="form-control" id="name" name="name">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="email" class="form-control" id="email" name="email">
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <label for="password">Password</label>
                                                    <input type="password" class="form-control" name="password"
                                                           id="password">
                                                </div>
                                                <div class="col">
                                                    <label for="password_confirmation">Password Confirmation</label>
                                                    <input type="password" class="form-control" id="password_confirmation"
                                                           name="password_confirmation">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-6">
                                            <p>Role</p>
                                            @foreach($roles as $role)
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" name="checkbox[]" value="{{ $role->id }}" class="custom-control-input" id="role{{ $role->id }}">
                                                <label class="custom-control-label" for="role{{ $role->id }}">{{ $role->name }}</label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only" style="font-size: 35px">Close</span>
                </button>
            </div>
        @endif
        <div>
            <table class="table table-hover table-responsive-sm table-responsive-md ">
                <thead class="thead">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Role</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <th>{{ $user->id }}</th>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            @foreach($user->roles as $role)
                                {{ $role->name }},
                            @endforeach
                        </td>
                        <td class="d-flex">
                            <!-- Button trigger modal -->
                            <a href="{{ route('user.edit',$user->id) }}" type="button" class="btn btn-success mr-2"> Edit </a>
                            <button data-toggle="modal" data-target="#delete{{ $user->id }}" class="btn btn-danger btn-del"> Del</button>
                            <div class="modal fade" id="delete{{ $user->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{ route('user.destroy',$user->id) }}" method="POST">
                                            {!! @method_field('DELETE') !!}
                                            @csrf
                                            <div class="modal-body">
                                                <h5> Do you want to delete</h5>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Delete</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
