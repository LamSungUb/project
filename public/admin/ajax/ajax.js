// Show image
function loadFile(event) {
    var image = document.getElementById('output');
    image.src = URL.createObjectURL(event.target.files[0]);
};

//function ajax
function ajaxForm(data, type, url) {
    return $.ajax({
        data: data,
        dataType: 'html',
        type: type,
        headers: {"X-CSRF-Token": $("meta[name='csrf-token']").attr("content")},
        contentType: false,
        processData: false,
        url: url
    })
}

setInterval(function () {
    $('.alert').hide(2000);
}, 3000);

