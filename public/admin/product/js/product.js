
// Create product ajax
$(document).on("click", '#btn_create', function (e) {
    e.preventDefault();
    var form_create = document.getElementById('form_create');
    var formData = new FormData(form_create);
    var url = $(this).data("href");

    ajaxForm(formData, type = "POST", url).done(function (data) {
        console.log(data)
        $('#modal_create').modal('hide');
        $("#tbl_result").empty();
        $('#tbl_result').append(data);
    })
});

//Del product ajax
$(document).on("click", '.btn-del', function (e) {
    e.preventDefault();
    var id = $(this).data("id");
    var url = $(this).data("href");
    var del = confirm('You sure want to delete');

    if (del == true) {
        ajaxForm(id, type = "DELETE", url).done(function (data) {
            $("#tbl_result").empty();
            $('#tbl_result').append(data);
        })
    }
});

//Edit product ajax
$(document).on("click", '.btn-edit', function (e) {
    e.preventDefault();
    var id = $(this).data("id");
    var url = $(this).data("href");

    ajaxForm(id, type = "GET", url).done(function (data) {
        $("#modal").empty();
        $('#modal').append(data);
        $('#modal_update').modal('show');
    })
});

//update product ajax
$(document).on("click", '#btn_update', function (e) {
    e.preventDefault();
    var form_update = document.getElementById('form_update');
    var formData = new FormData(form_update);
    var url = $(this).data("href");

    ajaxForm(formData, type = "POST", url).done(function (data) {
        $("#tbl_result").empty();
        $('#tbl_result').append(data);
        $('#modal_update').modal('hide');
    })
});

//Search product ajax
$('#search').keyup(function(){
    var keyword = $(this).val();
    var url = $(this).data("href") + "?keyword=" + keyword;
    if(keyword != '')
    {
        ajaxForm("", type = "GET", url).done(function (data) {
            $("#tbl_result").empty();
            $('#tbl_result').append(data);
        })
    }
});
